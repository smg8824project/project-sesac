# cough_detect
def print_cough_prediction(OUTPUT_FILENAME):
    WAVE_OUTPUT_FILENAME = OUTPUT_FILENAME +".wav"
    MFCC_OUTPUT_FILENAME = OUTPUT_FILENAME +".csv"
    MELS_OUTPUT_FILENAME = OUTPUT_FILENAME +".png"

    
    
    def del_file(OUTPUT_FILENAME):
        os.remove(WAVE_OUTPUT_FILENAME)
        os.remove(MFCC_OUTPUT_FILENAME)
        os.remove(MELS_OUTPUT_FILENAME)
        print("--------------------------")
        print("Del "+ WAVE_OUTPUT_FILENAME)
        print("Del "+ MFCC_OUTPUT_FILENAME)
        print("Del "+ MELS_OUTPUT_FILENAME)
        print("--------------------------")
        
        
    def move_file(OUTPUT_FILENAME):
        shutil.move(WAVE_OUTPUT_FILENAME, "./data_realtime/save")
        shutil.move(MFCC_OUTPUT_FILENAME, "./data_realtime/save")
        shutil.move(MELS_OUTPUT_FILENAME, "./data_realtime/save")
        print("--------------------------")
        print("Move "+ WAVE_OUTPUT_FILENAME)
        print("Move "+ MFCC_OUTPUT_FILENAME)
        print("Move "+ MELS_OUTPUT_FILENAME)
        print("--------------------------")

    def test_data(OUTPUT_FILENAME):
        MELS_OUTPUT_FILENAME = OUTPUT_FILENAME +".png"

        tocompose = T.Compose([
                T.Resize(255),
                T.CenterCrop(224),
                T.ToTensor()
            ])

        img_path = MELS_OUTPUT_FILENAME
        img_RGB  = Image.open(img_path).convert('RGB')
        img_compose = tocompose(img_RGB)
        img_data = torch.unsqueeze(img_compose, 0)

        is_cuda = torch.cuda.is_available()
        device = torch.device('cuda' if is_cuda else 'cpu')

        PATH = "./model/test/model_cough.pt"
        model = torch.load(PATH, map_location='cpu')

        #PATH = "./model/test/20220823_resnet18_90.pth"
        #model = torch.load(PATH)
    
        with torch.no_grad():
            logit = model(img_data)
            pred = logit.argmax(dim=1, keepdim=True)
        return pred
        
    print("--------------------------")
    print("Cough_predict_Start")

       
    # 모델 불러오기 
    predicted_vector = test_data(OUTPUT_FILENAME)
    
    if (str(predicted_vector[0]) == "tensor([1])"):
        print("기침소리 : 없음")  
        del_file(OUTPUT_FILENAME) 
    elif(str(predicted_vector[0]) == "tensor([0])"):
        print("기침소리 : 있음")  
        move_file(OUTPUT_FILENAME)  
        
    print("Cough_predict_End")
    print("--------------------------")




# melspectrum    
def extract_mels(OUTPUT_FILENAME):
    WAVE_OUTPUT_FILENAME = OUTPUT_FILENAME +".wav"
    MELS_OUTPUT_FILENAME = OUTPUT_FILENAME +".png"
    
    print("--------------------------")
    print("Extract_features_MELS_Start")
    
    y,sr = librosa.load(WAVE_OUTPUT_FILENAME, sr=44100, mono=True)
    
    melspec = librosa.feature.melspectrogram(y=y, sr=sr, n_mels=128)
    log_melspec = librosa.power_to_db(melspec, ref=np.max)  
    librosa.display.specshow(log_melspec, sr=sr)

    plt.savefig(MELS_OUTPUT_FILENAME)
    plt.clf()
    print("Extract_features_MELS_End")
    print("--------------------------")
    extract_mfcc(OUTPUT_FILENAME)



# mfcc
def extract_mfcc(OUTPUT_FILENAME):
    print("--------------------------")
    print("Extract_features_MFCC_Start")
    
    CFG = {
    'SR':16000,
    'N_MFCC':32, 
    'SEED':41
    }
    
    WAVE_OUTPUT_FILENAME = OUTPUT_FILENAME +".wav"
    MFCC_OUTPUT_FILENAME = OUTPUT_FILENAME +".csv"
    
    features = []
    y, sr = librosa.load(WAVE_OUTPUT_FILENAME, sr=CFG['SR'])
    mfcc = librosa.feature.mfcc(y=y, sr=sr, n_mfcc=CFG['N_MFCC'])
    
    y_feature = []
    
    for e in mfcc:
        y_feature.append(np.mean(e))
        
    features.append(y_feature)
    

    mfcc_df = pd.DataFrame(features, columns=['mfcc_'+str(x) for x in range(1,CFG['N_MFCC']+1)])
    mfcc_df.to_csv(MFCC_OUTPUT_FILENAME, index = False)


    print("Extract_features_MFCC_End")
    print("--------------------------")
    print_cough_prediction(OUTPUT_FILENAME)



# realtime sound save
def audio_rec():
    TIME_FILENAME = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    OUTPUT_PATH = "./data_realtime"
    if not os.path.exists(OUTPUT_PATH):
        os.mkdir(OUTPUT_PATH)
        os.mkdir(OUTPUT_PATH + "/save")

    OUTPUT_FILENAME = "./data_realtime/cough_test_"+TIME_FILENAME
    WAVE_OUTPUT_FILENAME = OUTPUT_FILENAME +".wav"

    CHUNK = 1024
    FORMAT = pyaudio.paInt16
    CHANNELS = 1
    RATE = 44100
    RECORD_SECONDS = 5

    p = pyaudio.PyAudio()

    stream = p.open(format=FORMAT,channels=CHANNELS,rate=RATE,input=True,frames_per_buffer=CHUNK)

    print("Start to record the audio.")

    frames = []

    for i in range(0, int(RATE / CHUNK * RECORD_SECONDS)):
        data = stream.read(CHUNK)
        frames.append(data)

    print("Recording is finished.")

    stream.stop_stream()
    stream.close()
    p.terminate()

    wf = wave.open(WAVE_OUTPUT_FILENAME, 'wb')
    wf.setnchannels(CHANNELS)
    wf.setsampwidth(p.get_sample_size(FORMAT))
    wf.setframerate(RATE)
    wf.writeframes(b''.join(frames))
    wf.close()
    
    print("Create " + WAVE_OUTPUT_FILENAME)

    extract_mels(OUTPUT_FILENAME)







# 완성기능 반복

import time

import pyaudio
import wave
import sys
import datetime

import librosa
import librosa.display
import matplotlib.pyplot as plt
import numpy as np

import pandas as pd

import torch
import torch.nn as nn
import joblib
import os

import torchvision
from torchvision import models
from torchvision.models import resnet50
import torchvision.transforms as T
from PIL import Image 

import shutil

while True:
    audio_rec()
    time.sleep(0.001)